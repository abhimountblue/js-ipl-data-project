const papa = require("papaparse")
const fs = require("fs")

const csvDeliveries = fs.readFileSync(
  "/home/abhishek/Desktop/ipl/src/data/deliveries.csv",
  "utf-8"
)

let jsonDeliveries = papa.parse(csvDeliveries, {
  header: true,
})
function highestTimeOnePlayerDismissedBynotherPlayer(jsonDeliveries) {
  let dismissedArray = jsonDeliveries.data.filter(
    (element) => element.player_dismissed !== ""
  )

  const mostDissmissedPlayer = dismissedArray.reduce((accumulator, element) => {
    if (typeof element.player_dismissed === "undefined") {
      return accumulator
    }
    if (!accumulator[element.player_dismissed]) {
      const dismissedByPlayerArray = dismissedArray.filter(
        (item) => item.player_dismissed === element.player_dismissed
      )
      const dismissedByPlayer = dismissedByPlayerArray.reduce(
        (accumulator, value) => {
          accumulator[value.bowler] = (accumulator[value.bowler] || 0) + 1
          return accumulator
        },
        {}
      )

      const mostdismissedByPlayer = Object.keys(dismissedByPlayer).reduce(
        (accumulator, key) => {
          if (
            dismissedByPlayer[key] > accumulator.mostdismissedByPlayerNumber
          ) {
            accumulator.mostdismissedByPlayerName = key
            accumulator.mostdismissedByPlayerNumber = dismissedByPlayer[key]
          }
          return accumulator
        },
        { mostdismissedByPlayerName: "", mostdismissedByPlayerNumber: 0 }
      )
      accumulator[element.player_dismissed] = {
        [mostdismissedByPlayer.mostdismissedByPlayerName]:
          mostdismissedByPlayer.mostdismissedByPlayerNumber,
      }
    }
    return accumulator
  }, {})
  return mostDissmissedPlayer
}

const result = highestTimeOnePlayerDismissedBynotherPlayer(jsonDeliveries)

fs.writeFileSync(
  "/home/abhishek/Desktop/ipl/src/public/output/8-highest-time-one-player-dismissed-by-another-player.json",
  JSON.stringify(result, null, 2)
)
