const papa = require("papaparse")
const fs = require("fs")

const csvDeliveries = fs.readFileSync(
  "/home/abhishek/Desktop/ipl/src/data/deliveries.csv",
  "utf-8"
)

let jsonDeliveries = papa.parse(csvDeliveries, {
  header: true,
})

function bowlerWithBestEconomyInSuperOber(jsonDeliveries) {
  // filter deliveries that deliver in super over
  const superOverArray = jsonDeliveries.data.filter(
    (element) => element.is_super_over === "1"
  )

  const bowlerObject = superOverArray.reduce(
    (accumulator, element) => {
      accumulator.deliveriesByBowler[element.bowler] =
        (accumulator.deliveriesByBowler[element.bowler] || 0) + 1
      accumulator.runsGivenByBowler[element.bowler] =
        (accumulator.runsGivenByBowler[element.bowler] || 0) +
        Number(element.total_runs)
      return accumulator
    },
    { deliveriesByBowler: {}, runsGivenByBowler: {} }
  )

  const bowlerEconomy = Object.keys(bowlerObject.deliveriesByBowler).map(
    (element) => {
      // creating an array name bowler Economy and passing an object contain name and economy
      return {
        name: element,
        economy:
          (bowlerObject.runsGivenByBowler[element] /
            bowlerObject.deliveriesByBowler[element]) *
          6,
      }
    }
  )

  bowlerEconomy.sort((a, b) => {
    // contains the economy of the bowler stored as a number
    return a.economy - b.economy
  })
  return bowlerEconomy[0]
}

const result = bowlerWithBestEconomyInSuperOber(jsonDeliveries)

fs.writeFileSync(
  "/home/abhishek/Desktop/ipl/src/public/output/9-bowler-with-best-economi-in-superover.json",
  JSON.stringify(result, null, 2)
)
