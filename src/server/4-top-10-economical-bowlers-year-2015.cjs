const papa = require("papaparse")
const fs = require("fs")

const csvDeliveries = fs.readFileSync(
  "/home/abhishek/Desktop/ipl/src/data/deliveries.csv",
  "utf-8"
)

const csvMatches = fs.readFileSync(
  "/home/abhishek/Desktop/ipl/src/data/matches.csv",
  "utf-8"
)

let jsonDeliveries = papa.parse(csvDeliveries, {
  header: true,
})
jsonDeliveries = jsonDeliveries.data

let jsonMatches = papa.parse(csvMatches, {
  header: true,
})

jsonMatches = jsonMatches.data

function top10EconomicalBowlerYear2015(jsonMatches) {

  const matchIdSeason = jsonMatches.reduce((accumulator, match) => {
    if (match.season === "2015") {
      accumulator[match.id] = match.season
    }
    return accumulator
  }, {})

  const allMatchesPlayed2015 = jsonDeliveries.filter((element) => {
    if (matchIdSeason[element.match_id]) {
      return true
    }
  })

  const bowlerObject = allMatchesPlayed2015.reduce((accumulator, element) => {
    if (typeof element.bowler === "undefined") {
      return accumulator
    }
    if (!accumulator[element.bowler]) {
      const totalDeliveriesByBowler = allMatchesPlayed2015.reduce(
        (acc, item) => {
          if (element.bowler === item.bowler) {
            return acc + 1
          }
          return acc
        },
        0
      )
      const totalRunsGivenByBowler = allMatchesPlayed2015.reduce(
        (acc, item) => {
          if (element.bowler === item.bowler) {
            return Number(item.total_runs) + acc
          }
          return acc
        },
        0
      )

      accumulator[element.bowler] =
        (totalRunsGivenByBowler / totalDeliveriesByBowler) * 6
    }
    return accumulator
  }, {})

  const bowlerEconomi = Object.keys(bowlerObject).reduce(
    (accumulator, element) => {
      accumulator.push({ name: element, economy: bowlerObject[element] })
      return accumulator
    },
    []
  )

  bowlerEconomi.sort((a, b) => {
    // contains the economy of the bowler stored as a number
    return a.economy - b.economy
  })
  const top10EconomicalBowler = bowlerEconomi.slice(0, 10)
  return top10EconomicalBowler
}

const result = top10EconomicalBowlerYear2015(jsonMatches)
console.log(result)

fs.writeFileSync(
  "/home/abhishek/Desktop/ipl/src/public/output/4-top-10-economical-bowlers-year-2015.json",
  JSON.stringify(result, null, 2)
)
