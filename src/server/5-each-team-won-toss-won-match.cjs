const path = require("path")
const papa = require("papaparse")

const fs = require("fs")

// reading csv file using fs module
const csvMatches = fs.readFileSync(
  "/home/abhishek/Desktop/ipl/src/data/matches.csv",
  "utf-8"
)
// converting csv to json using papaparse
let jsonMatches = papa.parse(csvMatches, {
  header: true,
})

jsonMatches = jsonMatches.data

function teamWonTossAndMatch(jsonMatches) {
  // using reduce function i will check same team won the toss and won the match than I will store in object with team name
  const teams = jsonMatches.reduce((accumulator, element) => {
    if (
      element.toss_winner === element.winner &&
      typeof element.winner !== "undefined"
    ) {
      accumulator[element.winner] = (accumulator[element.winner] || 0) + 1
    }
    return accumulator
  }, {})
  return teams
}
const result = teamWonTossAndMatch(jsonMatches)

// I have declare a path it will create a file in json formate using fs module
fs.writeFileSync(
  "/home/abhishek/Desktop/ipl/src/public/output/5-each-team-won-toss-won-match.json",
  JSON.stringify(teams, null, 2)
)
