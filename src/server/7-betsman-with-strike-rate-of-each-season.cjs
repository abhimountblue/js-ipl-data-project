const papa = require("papaparse")
const fs = require("fs")

const csvDeliveries = fs.readFileSync(
  "/home/abhishek/Desktop/ipl/src/data/deliveries.csv",
  "utf-8"
)

let jsonDeliveries = papa.parse(csvDeliveries, {
  header: true,
})
const csvMatches = fs.readFileSync(
  "/home/abhishek/Desktop/ipl/src/data/matches.csv",
  "utf-8"
)

let jsonMatches = papa.parse(csvMatches, {
  header: true,
})
jsonMatches = jsonMatches.data

function betsmanWithStrikeRateEachSeason(jsonMatches, jsonDeliveries) {
  const matchIdSeason = jsonMatches.reduce((accumulator, match) => {
    accumulator[match.id] = match.season
    return accumulator
  }, {})

  jsonDeliveries = jsonDeliveries.data.map((delivery) => {
    return { ...delivery, season: matchIdSeason[delivery.match_id] }
  })
  const allBatsManWithStrikeRate = jsonDeliveries.reduce(
    (accumulator, element) => {
      if (
        !accumulator[element.batsman] &&
        typeof element.batsman !== "undefined"
      ) {
        const allBallPlayedByBatsman = jsonDeliveries.filter(
          (item) => item.batsman === element.batsman
        )
        const allSeason = allBallPlayedByBatsman.reduce((acc, value) => {
          if (!acc[value.season]) {
            const seasonDeliveries = allBallPlayedByBatsman.filter(
              (data) => value.season === data.season
            )
            const allPlayedBallCount = seasonDeliveries.reduce(
              (acc, deliverys) => acc + 1,
              0
            )
            const allRunsByBatsman = seasonDeliveries.reduce(
              (acc, deliverys) => acc + Number(deliverys.batsman_runs),
              0
            )
            let strikeRate = (allRunsByBatsman / allPlayedBallCount) * 100
            acc[value.season] = strikeRate
          }
          return acc
        }, {})
        accumulator[element.batsman] = allSeason
      }
      return accumulator
    },
    {}
  )
  return allBatsManWithStrikeRate
}
const result = betsmanWithStrikeRateEachSeason(jsonMatches, jsonDeliveries)

fs.writeFileSync(
  "/home/abhishek/Desktop/ipl/src/public/output/7-betsman-with-strike-rate-of-each-season.json",
  JSON.stringify(result, null, 2)
)
