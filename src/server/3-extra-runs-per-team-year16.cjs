const papa = require("papaparse")
const fs = require("fs")

const csvDeliveries = fs.readFileSync(
  "/home/abhishek/Desktop/ipl/src/data/deliveries.csv",
  "utf-8"
)

let jsonDeliveries = papa.parse(csvDeliveries, {
  header: true,
})
jsonDeliveries = jsonDeliveries.data

const csvMatches = fs.readFileSync(
  "/home/abhishek/Desktop/ipl/src/data/matches.csv",
  "utf-8"
)
// pa
let jsonMatches = papa.parse(csvMatches, {
  header: true,
})

jsonMatches = jsonMatches.data

function extraRunsConcededByEveryTeamYear2016(jsonMatches) {
  // filtering all matches that played in 2016
  const yearOf2016Matches = jsonMatches.filter(
    (element) => element.season === "2016"
  )
  const teamObject = yearOf2016Matches.reduce((accumulator, element) => {
    if (element.team1 !== "undefined" && element.team2 !== "undefined") {
      if (!accumulator[element["team1"]]) {
        accumulator[element["team1"]] = 0
      }
      if (!accumulator[element["team2"]]) {
        accumulator[element["team2"]] = 0
      }
    }
   
    const matchDeliveri = jsonDeliveries.filter(
      (item) => item.match_id === element.id
    )

    const firstInning = matchDeliveri.filter((team) => team.inning === "1")

    const secondInning = matchDeliveri.filter((team) => team.inning === "2")

    // calculating first inning extra runs
    const firstInningExtraRuns = firstInning.reduce((acc, team) => {
      return Number(team.extra_runs) + acc
    }, 0)

    // calculating second inning extra runs
    const secondInningExtraRuns = secondInning.reduce((acc, team) => {
      return Number(team.extra_runs) + acc
    }, 0)
    // assigning extra runs given by teams with teams key in object
    accumulator[firstInning[0]["bowling_team"]] =
      accumulator[firstInning[0]["bowling_team"]] + firstInningExtraRuns
    accumulator[secondInning[0]["bowling_team"]] =
      accumulator[secondInning[0]["bowling_team"]] + secondInningExtraRuns
    return accumulator
  }, {})
  return teamObject
}

const result = extraRunsConcededByEveryTeamYear2016(jsonMatches)

// I have declare a path it will create a file in json formate using fs module
fs.writeFileSync(
  "/home/abhishek/Desktop/ipl/src/public/output/3-extraRunsPerTeamYear2016.json",
  JSON.stringify(result, null, 2)
)
