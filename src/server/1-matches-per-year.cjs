const path = require("path")
const papa = require("papaparse")

const fs = require("fs")

const csvMatches = fs.readFileSync(
  "/home/abhishek/Desktop/ipl/src/data/matches.csv",
  "utf-8"
)
// converting csv file to json using papa parse
const jsonMatches = papa.parse(csvMatches, {
  header: true,
})

function howManyMatchesPerYear(jsonMatches) {
  // iterating an array and checking season is presend or absent if absent than create a key in seasonPerMatches name as season and value will be occurence of matches count
  const seasonPerMatches = jsonMatches.data.reduce((accumulator, element) => {
    if (typeof element.season === "undefined") {
      return accumulator
    }
    if (accumulator.hasOwnProperty(element.season)) {
      accumulator[element.season] = accumulator[element.season] + 1
    } else {
      accumulator[element.season] = 1
    }
    return accumulator
  }, {})
  return seasonPerMatches
}

// our result will be store in result variable
const result = howManyMatchesPerYear(jsonMatches)

// I have declare a path it will create a file in json formate using fs module
fs.writeFileSync(
  "/home/abhishek/Desktop/ipl/src/public/output/1-matches-per-year.json",
  JSON.stringify(result, null, 2)
)

console.log(result)