const papa = require("papaparse")
const fs = require("fs")

const csvMatches = fs.readFileSync(
  "/home/abhishek/Desktop/ipl/src/data/matches.csv",
  "utf-8"
)
// converting csv file to json using papa parse
let jsonMatches = papa.parse(csvMatches, {
  header: true,
})
jsonMatches = jsonMatches.data

// created a function it will return occurence of every teams win in every season
function winsEveryTeamEverYear(data) {
  const teams = data.reduce((accumulator, element) => {
    if (typeof element.season === "undefined") {
      return accumulator
    }
    if (
      !accumulator.hasOwnProperty(element.team1) &&
      typeof element.team1 !== "undefined"
    ) {
      accumulator[element.team1] = {}
    }
    if (
      !accumulator.hasOwnProperty(element.team2) &&
      typeof element.team2 !== "undefined"
    ) {
      accumulator[element.team2] = {}
    }
    if (element.season && element.winner) {
      if (accumulator[element.winner][element.season]) {
        accumulator[element.winner][element.season] =
          accumulator[element.winner][element.season] + 1
      } else {
        accumulator[element.winner][element.season] = 1
      }
    }
    return accumulator
  }, {})
  return teams
}

const result = winsEveryTeamEverYear(jsonMatches)

// I have declare a path it will create a file in json formate using fs module
fs.writeFileSync(
  "/home/abhishek/Desktop/ipl/src/public/output/2-matches-won-team-per-year.json",
  JSON.stringify(result, null, 2)
)
